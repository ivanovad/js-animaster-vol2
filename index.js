(function main() {
  document.getElementById("fadeInPlay").addEventListener("click", function () {
    const block = document.getElementById("fadeInBlock");
    animaster().fadeIn(block, 5000);
  });

  document.getElementById("fadeOutPlay").addEventListener("click", function () {
    const block = document.getElementById("fadeOutBlock");
    animaster().fadeOut(block, 5000);
  });

  document
    .getElementById("moveAndHidePlay")
    .addEventListener("click", function () {
      const block = document.getElementById("moveAndHideBlock");
      let startAnimation = animaster().moveAndHide(5000, {
        x: 100,
        y: 20,
      });
      startAnimation.play(block);
      document
        .getElementById("moveAndHideReset")
        .addEventListener("click", function () {
          startAnimation.reset(block);
        });
    });

  document
    .getElementById("showAndHidePlay")
    .addEventListener("click", function () {
      const block = document.getElementById("showAndHideBlock");
      const showAndHidePlay = animaster().showAndHide(5000);
      showAndHidePlay.play(block);
    });

  document.getElementById("movePlay").addEventListener("click", function () {
    const block = document.getElementById("moveBlock");
    animaster().move(block, 1000, {
      x: 100,
      y: 10,
    });
  });

  document.getElementById("scalePlay").addEventListener("click", function () {
    const block = document.getElementById("scaleBlock");
    animaster().scale(block, 1000, 1.25);
  });

  const animation = {};
  document.getElementById("heartPlay").addEventListener("click", function () {
    const block = document.getElementById("heartBlock");
    animation.heart = animaster().heartBeating(block);
  });

  document.getElementById("heartStop").addEventListener("click", function () {
    animation.heart.stop(animation.heart.timerId);
  });

  document.getElementById("shakingPlay").addEventListener("click", function () {
    const block = document.getElementById("shakingBlock");
    animation.shaking = animaster().shaking(block);
  });
  document.getElementById("shakingStop").addEventListener("click", function () {
    animation.shaking.stop(animation.shaking.timerId);
  });
  
  document.getElementById('flipAndScalePlay')
  .addEventListener('click', function () {
    const block = document.getElementById("flipAndScaleBlock");
    animaster().flipAndScale(block, 5000, 150, 180);
  });
})();

function animaster() {
  /**
   * Блок плавно появляется из прозрачного.
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   */
  function fadeIn(element, duration) {
    element.style.transitionDuration = `${duration}ms`;
    element.classList.remove("hide");
    element.classList.add("show");
  }
  /**
   * Блок плавно исчезает до прозрачного.
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   */
  function fadeOut(element, duration) {
    element.style.transitionDuration = `${duration}ms`;
    element.classList.remove("show");
    element.classList.add("hide");
  }
  /**
   * Функция, передвигающая элемент
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param translation — объект с полями x и y, обозначающими смещение блока
   */
  function move(element, duration, translation) {
    element.style.transitionDuration = `${duration}ms`;
    element.style.transform = getTransform(translation, null);
  }
  /**
   * Функция, передвигающая элемент вправо,вниз и исчезающая
   * @param duration — Продолжительность анимации в миллисекундах
   * @param translation — объект с полями x и y, обозначающими смещение блока
   */

  function moveAndHide(duration, translation) {
    addMove((duration * 2) / 5, translation);
    addDelay((duration * 2) / 5);
    addFadeOut((duration * 3) / 5);
    return this;
    // reset: function (element) {
    //   resetFadeIn(element);
    //   resetFadeOut(element);
    //   resetMoveAndScale(element);
    // }
  }

  /**
   * Функция, появиться, подождать и исчезнуть
   * @param duration — Продолжительность анимации в миллисекундах
   * @param translation — объект с полями x и y, обозначающими смещение блока
   */
  function showAndHide(duration) {
    addFadeIn(duration / 3);
    addDelay(duration / 3);
    addFadeOut(duration / 3);
    return this;
  }

  /**
   * Функция, увеличивающая/уменьшающая элемент
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
   */
  function scale(element, duration, ratio) {
    element.style.transitionDuration = `${duration}ms`;
    element.style.transform = getTransform(null, ratio);
  }

  /**
   * Функция имитации сердцебиения
   */
  function heartBeating(element) {
    addScale(500, 1.4);
    addScale(500, 1);
    play(element, true);
  }
  /**
   * Функция дрожания элемента
   */
  function shaking(element) {
    addMove(250, { x: 20, y: 0 });
    addMove(250, { x: 0, y: 0 });
    play(element, true);
  }
  /**
   * Функция остановки анимации
   * @param timer — переменная-таймер который нужно сбросить
   */
  function stop(timer) {
    clearInterval(timer);
  }

  function resetFadeIn(element) {
    element.style.transitionDuration = null;
    element.classList.add("hide");
    element.classList.remove("show");
  }

  function resetFadeOut(element) {
    element.style.transitionDuration = null;
    element.classList.add("show");
    element.classList.remove("hide");
  }

  function resetMoveAndScale(element) {
    element.style.transitionDuration = null;
    element.style.transform = null;
  }
  /* Animaster vol.2 */
  /* новая анимация  */
  /**
   * Функция, вращает и увеличивает/уменьшает элемент
   * @param element — HTMLElement, который надо анимировать
   * @param duration — Продолжительность анимации в миллисекундах
   * @param zoom — Величина движения элемента по оси Z
   * @param degrees - Угол поворота элемента
   */
  function flipAndScale(element, duration, zoom, degrees) {
    element.style.transitionDuration = `${duration/2}ms`;
    element.style.transform = `perspective(400px) translateZ(${zoom}px) rotateY(${degrees}deg)`;
    setTimeout(() => element.style.transform = `perspective(400px) translateZ(0px) rotateY(0deg)`, duration/2);
  }

  const _steps = [];
  function addScale(duration, ratio) {
    _steps.push({
      name: "scale",
      duration,
      ratio,
    });
    return this;
  }
  function addMove(duration, translation) {
    _steps.push({
      name: "move",
      duration,
      translation,
    });
    return this;
  }
  function addFadeIn(duration) {
    _steps.push({
      name: "fadeIn",
      duration,
    });
    return this;
  }
  function addFadeOut(duration) {
    _steps.push({
      name: "fadeOut",
      duration,
    });
    return this;
  }

  function addFlipAndScale(duration, zoom, degrees) {
    this._steps.push({
      name: 'flipAndScale',
      duration,
      zoom,
      degrees
    });
    return this;
  }

  function delay(duration) {
    setTimeout(() => {}, duration);
  }

  function addDelay(duration) {
    _steps.push({
      name: "delay",
      duration,
    });
    return this;
  }

  function play(element, cycled = false, startedTime = 0) {
    let timeOut = startedTime;

    for (let i = 0; i < _steps.length; i++) {
      switch (_steps[i].name) {
        case "scale":
          setTimeout(
            () => scale(element, _steps[i].duration, _steps[i].ratio),
            timeOut
          );
          timeOut += _steps[i].duration;
          break;
        case "move":
          setTimeout(
            () => move(element, _steps[i].duration, _steps[i].translation),
            timeOut
          );
          timeOut += _steps[i].duration;
          break;
        case "fadeIn":
          setTimeout(() => fadeIn(element, _steps[i].duration), timeOut);
          timeOut += _steps[i].duration;
          break;
        case "fadeOut":
          setTimeout(() => fadeOut(element, _steps[i].duration), timeOut);
          timeOut += _steps[i].duration;
          break;
        case 'flipAndScale':
          setTimeout(() => flipAndScale(element, _steps[i].duration, _steps[i].zoom, _steps[i].degrees), timeOut);
          break;
        case "delay":
          setTimeout(() => delay(_steps[i].duration), timeOut);
          timeOut += _steps[i].duration;
          break;
      }
    }

    if (cycled) {
      play(element, true, timeOut);
    }
  }

  return {
    _steps,
    fadeIn,
    fadeOut,
    move,
    moveAndHide,
    showAndHide,
    scale,
    heartBeating,
    shaking,
    flipAndScale,
    addFlipAndScale,
    addScale,
    addMove,
    addFadeIn,
    addFadeOut,
    addDelay,
    play,
  };
}

function getTransform(translation, ratio) {
  const result = [];
  if (translation) {
    result.push(`translate(${translation.x}px,${translation.y}px)`);
  }
  if (ratio) {
    result.push(`scale(${ratio})`);
  }
  return result.join(" ");
}

function resetFadeIn() {}
